/*
 *  FuelCanister
 *  Copyright (C) 2021  456Xander
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package at.xander.register;

import at.xander.FuelCanisterMod;
import at.xander.item.FuelCanisterItem;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

import java.util.function.Function;

public class FuelCanisterItems {
	private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(BuiltInRegistries.ITEM,
			FuelCanisterMod.MODID);


	// Helper Functions
	private ResourceKey<Item> itemId(String name) {
		return ResourceKey.create(Registries.ITEM, ResourceLocation.fromNamespaceAndPath(FuelCanisterMod.MODID, name));
	}

	private DeferredHolder<Item, Item> registerItem(String name, Function<Item.Properties, Item> constructor) {
		return registerItem(name, constructor, new Item.Properties());
	}

	private DeferredHolder<Item, Item> registerItem(String name, Function<Item.Properties, Item> constructor, Item.Properties props) {
		var propWithId = props.setId(itemId(name));
		return ITEMS.register(name, () -> constructor.apply(propWithId));
	}


	public final DeferredHolder<Item, Item> FuelCanister = registerItem("fuel_canister", FuelCanisterItem::newNormal);
	public final DeferredHolder<Item, Item> CreativeFuelCanister = registerItem("creative_fuel_canister", FuelCanisterItem::newCreative);

	public FuelCanisterItems(IEventBus bus) {
		ITEMS.register(bus);
		CustomDataComponents.init(bus);
		bus.addListener(this::handleCreativeTabRegistration);
	}

	private void handleCreativeTabRegistration(BuildCreativeModeTabContentsEvent event) {
		if (event.getTabKey() == CreativeModeTabs.TOOLS_AND_UTILITIES) {
			event.accept(FuelCanister.get());
			event.accept(CreativeFuelCanister.get());
		}
	}
}
