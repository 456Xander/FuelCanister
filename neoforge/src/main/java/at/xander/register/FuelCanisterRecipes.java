package at.xander.register;

import at.xander.FuelCanisterMod;
import at.xander.item.FuelCanisterRefillRecipe;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;

public class FuelCanisterRecipes {

	private static final DeferredRegister<RecipeSerializer<?>> RECIPES = DeferredRegister
			.create(BuiltInRegistries.RECIPE_SERIALIZER, FuelCanisterMod.MODID);

	@SuppressWarnings("unused")
	private static final DeferredHolder<RecipeSerializer<?>, RecipeSerializer<?>> refillRec = RECIPES.register("refill_recipe",
			() -> FuelCanisterRefillRecipe.SERIALIZER);

	public static void init(IEventBus bus) {
		RECIPES.register(bus);
	}
}
