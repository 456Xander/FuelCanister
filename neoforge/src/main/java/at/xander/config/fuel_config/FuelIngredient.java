package at.xander.config.fuel_config;

import net.minecraft.core.HolderLookup;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public interface FuelIngredient {
    boolean matches(ItemStack itemStack, HolderLookup<Item> hl);
}
