/*
 *  FuelCanister
 *  Copyright (C) 2021  456Xander
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package at.xander.config;

import java.util.List;

import at.xander.config.fuel_config.FuelConfig;
import com.google.common.collect.ImmutableList;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.neoforged.neoforge.common.ModConfigSpec;

public class Config {
	private static final int DEFAULT_CAPACITY = 4096;

	private ModConfigSpec config;

	private ModConfigSpec.Builder builder;

	private ModConfigSpec.IntValue capacity;
	private ModConfigSpec.BooleanValue craftAnyItem;
	private ModConfigSpec.ConfigValue<List<? extends String>> autoFuels;

	private static List<String> getDefaultAutoFuels() {
		ImmutableList.Builder<String> builder = new ImmutableList.Builder<>();
		builder.add(Items.COAL.toString().toString());
		builder.add(createEscapedBasicIngredient(Items.CHARCOAL));
		builder.add(createEscapedBasicIngredient(Items.COAL_BLOCK));
		return builder.build();
	}

	private static String createEscapedBasicIngredient(Item item) {
		return createEscapedBasicIngredient("item", item.toString().toString());
	}

	private static String createEscapedBasicIngredient(String type, String id) {
		return "{" + type + ": \"" + id + "\"}";
	}

	private static boolean autoFuelElementValidator(Object obj) {
		/*
		 * We don't check if the Items do really exist at this point, because this would
		 * not be realistic. It would be possible, that an item will only be registered
		 * after the config is loaded
		 */
		return obj instanceof String;
	}

	/**
	 * Create a new Config Object. Does not load any Configuration yet.
	 */
	public Config() {
		builder = new ModConfigSpec.Builder();
		builder.push("General");
		capacity = builder
				.comment("The capacity defines how many items a full FuelCanister can smelt (default ="
						+ DEFAULT_CAPACITY + " = " + (DEFAULT_CAPACITY / (64 * 8)) + " Stacks Coal)")
				.defineInRange("capacity", DEFAULT_CAPACITY, 0, Short.MAX_VALUE);
		builder.pop();
		builder.push("Fuels");
		builder.comment(
				"A List of Fuels, that will be automatically consumed when right-clicking with the FuelCanister in Hand",
				"Be careful about which items you put in here, as this can have unwanted consequences! (e.g. putting wood here uses up all your wood when using a FuelCanister)",
				"Format of each List Entry: Recipe-Like Json. e.g. {item: \"name\"} or {tag: \"TagName\"}",
				"For a simple item without metadata, just the item-id in the form mod_id:item_id is enough");
		autoFuels = builder.defineList("autoFuels", Config::getDefaultAutoFuels, Config::autoFuelElementValidator);
		builder.comment(
				"If a player should be able to use any Item with a fuel value when using a crafting table to fill the FuelCanister, or only those defined by autoFuels");
		craftAnyItem = builder.define("craftAnyFuel", true);
		builder.pop();
	}

	/**
	 * Get the Config Instance for registering it with Forge. When called the first
	 * time, the config is also built
	 * 
	 */
	public ModConfigSpec getConfig() {
		if (config == null && builder != null) {
			config = builder.build();
			builder = null;
		}
		return config;
	}

	public int getCapacity() {
		checkLoaded();
		return capacity.get();
	}

	public boolean getCraftAnyItem() {
		checkLoaded();
		return craftAnyItem.get();
	}

	public FuelConfig createFuelConfig() {
		checkLoaded();
		return new FuelConfig(autoFuels.get());
	}

	private void checkLoaded() {
		if (config == null) {
			throw new IllegalStateException("Config is not loaded yet, call getOrLoadConfig before");
		}
	}
}
