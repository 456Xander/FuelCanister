package at.xander.item;

import com.mojang.serialization.MapCodec;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.codec.StreamCodec;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.*;
import net.minecraft.world.level.block.entity.FuelValues;
import org.jetbrains.annotations.NotNull;

import at.xander.FuelCanisterMod;
import at.xander.config.FuelCanisterConfiguration;
import net.minecraft.core.NonNullList;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

import java.util.Objects;

public class FuelCanisterRefillRecipe implements CraftingRecipe {

	public static final Serializer SERIALIZER = new Serializer();

	private transient FuelValues cachedFuelValues = null;

	public FuelCanisterRefillRecipe() {
		super();
	}

	@Override
	public boolean matches(CraftingInput inv, Level worldIn) {

		// early check to abort if we don't have a fuelCanister to speed up CraftingRecipe scanning.
		var FCCount = inv.items().stream().
				filter(i -> i.getItemHolder().is(Objects.requireNonNull(FuelCanisterMod.instance().items.FuelCanister.getKey())))
				.count();
		if (FCCount != 1) {
			return false;
		}

		ItemStack fcan = null;
		int fuelFilling = 0;
		var fv = worldIn.fuelValues();
		var hl = worldIn.holderLookup(Registries.ITEM);
		// First find the FuelCanister Item
		for (int i = 0; i < inv.size(); i++) {
			ItemStack stack = inv.getItem(i);
			if (stack.getItem() instanceof FuelCanisterItem) {
				fcan = stack;
			} else if (isValidFuel(stack, fv, hl)) {
				fuelFilling += getFuelValue(stack, fv);
			} else if (!stack.isEmpty()) {
				// Error
				return false;
			}
		}
		// If a FuelCanister was found => matches
		if (fcan == null) {
			return false;
		}
		FuelCanisterItem theItem = (FuelCanisterItem) fcan.getItem();
		int fuelRem = theItem.getFuelRemaining(fcan);
		int maxFuel = theItem.getMaxFuel();
		int spaceLeft = maxFuel - fuelRem;

		cachedFuelValues = worldIn.fuelValues();
		return spaceLeft >= fuelFilling;
	}

	private int getFuelValue(ItemStack stack, FuelValues fv) {
		return stack.getBurnTime(RecipeType.SMELTING, fv) / FuelCanisterItem.FuelEqualsBurnTime;
	}

	private boolean isValidFuel(ItemStack stack, FuelValues fv, HolderLookup<Item> hl) {
		final FuelCanisterConfiguration config = FuelCanisterMod.instance().getConfig();
		return getFuelValue(stack, fv) > 0 && (config.craftingWithAnyFuel || config.fuels.apply(stack, hl));
	}

	@Override
	public boolean isSpecial() {
		return true;
	}

	@Override
	// Override this Method, because we don't want the FuelCanister to get left in
	// the Crafting Inventory
	public @NotNull NonNullList<ItemStack> getRemainingItems(CraftingInput inv) {
		NonNullList<ItemStack> nonnulllist = NonNullList.withSize(inv.size(), ItemStack.EMPTY);

		for (int i = 0; i < nonnulllist.size(); ++i) {
			ItemStack item = inv.getItem(i);
			ItemStack remainder = item.getCraftingRemainder();
			if (!remainder.isEmpty() && !(item.getItem() instanceof FuelCanisterItem)) {
				nonnulllist.set(i, remainder);
			}
		}

		return nonnulllist;
	}

	@Override
	public ItemStack assemble(CraftingInput inv, HolderLookup.Provider reg) {
		final var fv = cachedFuelValues;
		ItemStack fcan = null;
		int fuelFilling = 0;
		var hl = reg.lookupOrThrow(Registries.ITEM);
		// First find the FuelCanister Item
		for (int i = 0; i < inv.size(); i++) {
			ItemStack stack = inv.getItem(i);
			if (stack.getItem() instanceof FuelCanisterItem) {
				fcan = stack;
			} else if (stack.isEmpty()) {
				// Ignore empty slot
			} else if (isValidFuel(stack, fv, hl)) {
				fuelFilling += getFuelValue(stack, fv);
			} else {
				// Error
				// TODO log
				return ItemStack.EMPTY;
			}
		}
		// If a FuelCanister was found => matches
		if (fcan == null) {
			return ItemStack.EMPTY;
		}
		FuelCanisterItem theItem = (FuelCanisterItem) fcan.getItem();
		ItemStack resultStack = fcan.copy();
		int fuelRem = theItem.getFuelRemaining(resultStack);
		int maxFuel = theItem.getMaxFuel();
		int spaceLeft = maxFuel - fuelRem;
		if (spaceLeft < fuelFilling) {
			return ItemStack.EMPTY;
		}

		theItem.setFuelRemaining(resultStack, fuelRem + fuelFilling);

		return resultStack;
	}

	@Override
	public RecipeSerializer<? extends CraftingRecipe> getSerializer() {
		return SERIALIZER;
	}

	@Override
	public PlacementInfo placementInfo() {
		return PlacementInfo.NOT_PLACEABLE;
	}

	public static class Serializer implements RecipeSerializer<FuelCanisterRefillRecipe> {
		private static final FuelCanisterRefillRecipe INSTANCE = new FuelCanisterRefillRecipe();
		private static final MapCodec<FuelCanisterRefillRecipe> CODEC = MapCodec.unit(INSTANCE);
		private static final StreamCodec<RegistryFriendlyByteBuf, FuelCanisterRefillRecipe> STREAM_CODEC = StreamCodec.unit(INSTANCE);

		@Override
		public @NotNull MapCodec<FuelCanisterRefillRecipe> codec() {
			return CODEC;
		}

		@Override
		public @NotNull StreamCodec<RegistryFriendlyByteBuf, FuelCanisterRefillRecipe> streamCodec() {
			return STREAM_CODEC;
		}
	}

	@Override
	public @NotNull CraftingBookCategory category() {
		return CraftingBookCategory.MISC;
	}
}
