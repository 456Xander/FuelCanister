/*
 *  FuelCanister
 *  Copyright (C) 2021  456Xander
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package at.xander;

import java.util.logging.Logger;

import at.xander.config.Config;
import at.xander.config.FuelCanisterConfiguration;
import at.xander.config.fuel_config.FuelConfig;
import at.xander.register.FuelCanisterItems;
import at.xander.register.FuelCanisterRecipes;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.config.ModConfigEvent;

@Mod(FuelCanisterMod.MODID)
public class FuelCanisterMod {
	public static final String MODID = "fuel_canister";

	public Logger logger;

	private final Config config;

	private FuelCanisterConfiguration loadedConfig;

	private static FuelCanisterMod instance;
	
	public final FuelCanisterItems items;

	public static FuelCanisterMod instance() {
		return instance;
	}

	public FuelCanisterMod(ModContainer modContainer) {
		instance = this;
		loadedConfig = new FuelCanisterConfiguration(0, false, FuelConfig.EMPTY);
		logger = Logger.getLogger(MODID);
		modContainer.getEventBus().addListener(this::onConfigLoaded);
		config = new Config();
		modContainer.registerConfig(ModConfig.Type.COMMON, config.getConfig());
		items = new FuelCanisterItems(modContainer.getEventBus());
		FuelCanisterRecipes.init(modContainer.getEventBus());
	}


	@SubscribeEvent
	public void onConfigLoaded(ModConfigEvent e) {
		loadedConfig = new FuelCanisterConfiguration(config.getCapacity(), config.getCraftAnyItem(),
				config.createFuelConfig());
	}

	public FuelCanisterConfiguration getConfig() {
		return loadedConfig;
	}
}
