/*
 *  FuelCanister
 *  Copyright (C) 2021  456Xander
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package at.xander.register;

import at.xander.FuelCanisterMod;
import at.xander.item.FuelCanisterItem;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Function;

public class FuelCanisterRegistry {
    // Registries
    private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
            FuelCanisterMod.MODID);

    // Helper Functions
    private ResourceKey<Item> itemId(String name) {
        return ResourceKey.create(Registries.ITEM, ResourceLocation.fromNamespaceAndPath(FuelCanisterMod.MODID, name));
    }

    private RegistryObject<Item> registerItem(String name, Function<Item.Properties, Item> constructor) {
        return registerItem(name, constructor, new Item.Properties());
    }

    private RegistryObject<Item> registerItem(String name, Function<Item.Properties, Item> constructor, Item.Properties props) {
        var propWithId = props.setId(itemId(name));
        return ITEMS.register(name, () -> constructor.apply(propWithId));
    }


    public final RegistryObject<Item> FuelCanister = registerItem("fuel_canister",
            FuelCanisterItem::newNormal, new Item.Properties());
    public final RegistryObject<Item> CreativeFuelCanister = registerItem("creative_fuel_canister",
            FuelCanisterItem::newCreative, new Item.Properties());


    public FuelCanisterRegistry(FMLJavaModLoadingContext ctx) {
        ITEMS.register(ctx.getModEventBus());
        CustomDataComponents.init(ctx.getModEventBus());
        ctx.getModEventBus().addListener(this::handleCreativeTabRegistration);
    }

    private void handleCreativeTabRegistration(BuildCreativeModeTabContentsEvent event) {
        if (event.getTabKey() == CreativeModeTabs.TOOLS_AND_UTILITIES) {
            event.accept(FuelCanister.get());
            event.accept(CreativeFuelCanister.get());
        }
    }
}
