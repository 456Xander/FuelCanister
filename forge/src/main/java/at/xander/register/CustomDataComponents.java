package at.xander.register;

import at.xander.FuelCanisterMod;
import io.netty.buffer.ByteBuf;
import net.minecraft.core.component.DataComponentType;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.codec.ByteBufCodecs;
import net.minecraft.util.ExtraCodecs;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.UnaryOperator;

public class CustomDataComponents {

    private static final DeferredRegister<DataComponentType<?>> REGISTRAR = DeferredRegister.create(Registries.DATA_COMPONENT_TYPE, FuelCanisterMod.MODID);

    private static <T> RegistryObject<DataComponentType<T>> register(String name, UnaryOperator<DataComponentType.Builder<T>> factory) {
        var builder = factory.apply(DataComponentType.builder());
        return REGISTRAR.register(name, builder::build);
    }

    public static final RegistryObject<DataComponentType<Integer>> FUEL = register("fuel", builder -> builder.persistent(ExtraCodecs.NON_NEGATIVE_INT).networkSynchronized(ByteBufCodecs.VAR_INT));

    public static void init(IEventBus eventBus) {
        REGISTRAR.register(eventBus);
    }

}
