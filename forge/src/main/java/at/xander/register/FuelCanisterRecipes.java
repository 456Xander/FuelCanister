package at.xander.register;

import at.xander.FuelCanisterMod;
import at.xander.item.FuelCanisterRefillRecipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class FuelCanisterRecipes {

	private static final DeferredRegister<RecipeSerializer<?>> RECIPES = DeferredRegister
			.create(ForgeRegistries.RECIPE_SERIALIZERS, FuelCanisterMod.MODID);

	@SuppressWarnings("unused")
	private static final RegistryObject<RecipeSerializer<?>> refillRec = RECIPES.register("refill_recipe",
			() -> FuelCanisterRefillRecipe.SERIALIZER);

	public static void init(FMLJavaModLoadingContext ctx) {
		RECIPES.register(ctx.getModEventBus());
	}
}
