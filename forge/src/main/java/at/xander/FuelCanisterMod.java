/*
 *  FuelCanister
 *  Copyright (C) 2021  456Xander
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package at.xander;

import java.util.logging.Logger;

import at.xander.config.Config;
import at.xander.config.FuelCanisterConfiguration;
import at.xander.config.fuel_config.FuelConfig;
import at.xander.register.FuelCanisterRegistry;
import at.xander.register.FuelCanisterRecipes;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig.Type;
import net.minecraftforge.fml.event.config.ModConfigEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.jetbrains.annotations.NotNull;

@Mod(FuelCanisterMod.MODID)
public class FuelCanisterMod {
	public static final String MODID = "fuel_canister";

	public Logger logger;

	private Config config;

	private FuelCanisterConfiguration loadedConfig;

	private static FuelCanisterMod instance;
	
	public final @NotNull FuelCanisterRegistry items;

	public static @NotNull FuelCanisterMod instance() {
		return instance;
	}

	public FuelCanisterMod(FMLJavaModLoadingContext ctx) {
		instance = this;
		loadedConfig = new FuelCanisterConfiguration(0, false, FuelConfig.EMPTY);
		logger = Logger.getLogger(MODID);
		IEventBus bus = ctx.getModEventBus();
		bus.addListener(this::onConfigLoaded);
		config = new Config();
		ctx.registerConfig(Type.COMMON, config.getConfig());
		items = new FuelCanisterRegistry(ctx);
		FuelCanisterRecipes.init(ctx);
	}

	@SubscribeEvent
	public void onConfigLoaded(ModConfigEvent e) {
		loadedConfig = new FuelCanisterConfiguration(config.getCapacity(), config.getCraftAnyItem(),
				config.createFuelConfig());
	}

	public FuelCanisterConfiguration getConfig() {
		return loadedConfig;
	}
}
