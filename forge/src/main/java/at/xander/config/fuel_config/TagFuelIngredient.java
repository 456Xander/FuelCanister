package at.xander.config.fuel_config;

import at.xander.FuelCanisterMod;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import org.apache.logging.log4j.LogManager;

public class TagFuelIngredient implements FuelIngredient {

    private final TagKey<Item> tag;

    public TagFuelIngredient(ResourceLocation tag_id) {
        this.tag = TagKey.create(Registries.ITEM, tag_id);
    }

    @Override
    public boolean matches(ItemStack itemStack, HolderLookup<Item> hl) {
        var allItems = hl.get(tag);
        if(allItems.isPresent()) {
            return allItems.get().contains(itemStack.getItemHolder());
        } else {
            LogManager.getLogger(FuelCanisterMod.MODID).warn("Tag {} could not be found", this.tag.toString());
            return false;
        }
    }
}
