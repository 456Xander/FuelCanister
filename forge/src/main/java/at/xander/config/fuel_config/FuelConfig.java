/*
 *  FuelCanister
 *  Copyright (C) 2021  456Xander
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package at.xander.config.fuel_config;

import com.google.common.collect.ImmutableList;
import net.minecraft.core.HolderLookup;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

import java.util.List;
import java.util.Optional;

/**
 * FuelConfig is loaded by the {@link Config} by using
 * {@link Config#createFuelConfig()}. <br>
 * The FuelConfig is used to check, if an ItemStack is a valid Fuel for the
 * FuelCanister
 */
public class FuelConfig{
	private final List<FuelIngredient> ingredients;

	public static final FuelConfig EMPTY = new FuelConfig();

	private FuelConfig() {
		ingredients = ImmutableList.of();
	}

	public FuelConfig(List<? extends String> fuelList) {
		ingredients = fuelList.stream().map(FuelIngredientParser::parse).filter(Optional::isPresent).map(Optional::get)
				.toList();
	}

	/**
	 * Checks if the ItemStack is valid to use for this FuelConfig.
	 */
	public boolean apply(ItemStack stack, HolderLookup<Item> hl) {
		// Have to search all ingredients
		for (FuelIngredient i : ingredients) {
			if (i.matches(stack, hl)) {
				return true;
			}
		}
		return false;
	}
}
