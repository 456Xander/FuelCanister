package at.xander.config.fuel_config;

import at.xander.FuelCanisterMod;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.resources.ResourceLocation;
import org.apache.logging.log4j.LogManager;

import java.util.Optional;

public class FuelIngredientParser {

    private static Optional<FuelIngredient> tryParseNew(String str) {
        // new Syntax:
        // Item => mod:id
        // Tag => #mod:id

        // some basic validation:
        if (!str.contains(":") || str.contains("{") || str.contains("}")) {
            return Optional.empty();
        }
        if (str.startsWith("#")) {
            return Optional.of(new TagFuelIngredient(ResourceLocation.parse(str.substring(1))));
        } else {
            return Optional.of(new ItemFuelIngredient(ResourceLocation.parse(str)));
        }
    }

    private static Optional<FuelIngredient> tryParseOld(String str) {
        // old Syntax:
        // Item => {"item": "mod:id"}
        // Tag => {"tag": "mod:id"}
        try {
            var obj = new Gson().fromJson(str, JsonObject.class);
            if (obj.has("item")) {
                return Optional.of(new ItemFuelIngredient(ResourceLocation.parse(obj.get("item").getAsString())));
            } else if (obj.has("tag")) {
                return Optional.of(new TagFuelIngredient(ResourceLocation.parse(obj.get("item").getAsString())));
            } else {
                LogManager.getLogger(FuelCanisterMod.MODID).warn("Ingredient JSON must contain \"item\" or \"tag\" key: {}", str);
                return Optional.empty();
            }
        } catch (JsonSyntaxException e) {
            LogManager.getLogger(FuelCanisterMod.MODID).warn("Ingredient is neither new style nor valid JSON: {}", str);
            return Optional.empty();
        }
    }

    static Optional<FuelIngredient> parse(String str) {
        String trimmed = str.trim();
        return tryParseNew(trimmed).or(() -> tryParseOld(trimmed));
    }

}
