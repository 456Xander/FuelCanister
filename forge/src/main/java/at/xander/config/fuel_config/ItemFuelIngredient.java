package at.xander.config.fuel_config;

import at.xander.FuelCanisterMod;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import org.apache.logging.log4j.LogManager;


public class ItemFuelIngredient  implements FuelIngredient {
    private final ResourceKey<Item> item_id;

    public ItemFuelIngredient(ResourceLocation item_id) {
        this.item_id = ResourceKey.create(Registries.ITEM, item_id);
    }

    @Override
    public boolean matches(ItemStack itemStack, HolderLookup<Item> hl) {
        var matchingItem = hl.get(this.item_id);
        if(matchingItem.isPresent()) {
            return itemStack.getItemHolder().is(matchingItem.get().key());
        } else  {
            LogManager.getLogger(FuelCanisterMod.MODID).warn("Item ID {} does not exist", this.item_id.toString());
            return false;
        }
    }
}
