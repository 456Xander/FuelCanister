package at.xander.config;

import at.xander.config.fuel_config.FuelConfig;

public class FuelCanisterConfiguration {
	public final int capacity;
	public final boolean craftingWithAnyFuel;
	public final FuelConfig fuels;

	public FuelCanisterConfiguration(int capacity, boolean craftingWithAnyFuel, FuelConfig fuels) {
		super();
		this.capacity = capacity;
		this.craftingWithAnyFuel = craftingWithAnyFuel;
		this.fuels = fuels;
	}

}
