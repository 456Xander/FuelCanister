package at.xander.item;

import com.google.common.collect.ImmutableMap;
import net.minecraft.core.component.DataComponentMap;
import net.minecraft.core.component.DataComponentType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

public class LazyDataComponentMap implements DataComponentMap {

    /**
     * invariant: each Mapping maps a <code>DataComponentType</code> to a <code>Supplier</code> of a compatible data type.
     * This invariant is constructed by the Builder and maintained as suppliers is immutable.
     */
    private final Map<DataComponentType, Supplier> suppliers;

    protected LazyDataComponentMap(Map<DataComponentType, Supplier> suppliers) {
        this.suppliers = suppliers;
    }

    @Override
    public @Nullable <T> T get(@NotNull DataComponentType<? extends T> key) {
        if (suppliers.containsKey(key)) {
            return (T) suppliers.get(key).get();
        }
        return null;
    }

    @Override
    public Set<DataComponentType<?>> keySet() {
        return Set.of();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final ImmutableMap.Builder<DataComponentType, Supplier> mapBuilder = new ImmutableMap.Builder<>();

        public <T> Builder add(DataComponentType<T> key, Supplier<? extends T> value) {
            mapBuilder.put(key, value);
            return this;
        }

        public LazyDataComponentMap build() {
            return new LazyDataComponentMap(mapBuilder.build());
        }
    }
}
