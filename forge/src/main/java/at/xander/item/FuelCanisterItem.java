/*
 *  FuelCanister
 *  Copyright (C) 2021  456Xander
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package at.xander.item;

import javax.annotation.Nullable;

import at.xander.FuelCanisterMod;
import at.xander.config.fuel_config.FuelConfig;
import at.xander.register.CustomDataComponents;
import net.minecraft.core.component.DataComponentMap;
import net.minecraft.core.component.DataComponents;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.NotNull;


/*
 * Item for filled fuel Canister
 *
 * This Item uses the Damage of an ItemStack to represent how many uses are left instead of how "damaged" the Item is.
 * So an almost empty fuel Canister will have Damage 0
 *
 * That way, when a User updates the Config no additional uses are created, since only the max damage is changed
 */
public class FuelCanisterItem extends Item {

    protected static final int FuelEqualsBurnTime = 50;
    protected static final int FuelUsedPerBurn = 200 / FuelEqualsBurnTime;

    private final boolean alwaysFull;

    @Nullable
    private transient String emptyTranslationKey;
    @Nullable
    private transient String creativeTranslationKey;

    private DataComponentMap cachedComponents = null;

    public FuelCanisterItem(Properties properties, boolean creative) {
        super(properties.stacksTo(1));
        this.alwaysFull = creative;
    }

    public static FuelCanisterItem newCreative(Properties properties) {
        return new FuelCanisterItem(properties, true);
    }
    public static FuelCanisterItem newNormal(Properties properties) {
        return new FuelCanisterItem(properties, false);
    }

    @Override
    public @NotNull DataComponentMap components() {
        if (cachedComponents == null) {
            var builder = LazyDataComponentMap.builder();
            builder = builder.add(DataComponents.MAX_DAMAGE, () -> FuelCanisterMod.instance().getConfig().capacity);
            if (!alwaysFull) {
                builder = builder.add(DataComponents.DAMAGE, () -> FuelCanisterMod.instance().getConfig().capacity);
            }
            var maxDamageOverride = builder.build();
            cachedComponents = DataComponentMap.composite(super.components(), maxDamageOverride);
        }
        return cachedComponents;
    }

    @Override
    public ItemStack getCraftingRemainder(ItemStack itemStack) {
        int fuelRemaining = getFuelRemaining(itemStack);
        ItemStack newStack = itemStack.copy();
        // Make sure fuelRemaining never gets negative
        setFuelRemaining(newStack, Math.max(fuelRemaining - FuelUsedPerBurn, 0));
        return newStack;
    }

    @Override
    public int getBurnTime(ItemStack itemStack, RecipeType<?> recipeType) {
        if (getFuelRemaining(itemStack) >= FuelUsedPerBurn) {
            return 200;
        }
        return 0;
    }

    @Override
    public @NotNull InteractionResult use(@NotNull Level level, Player player, @NotNull InteractionHand hand) {
        ItemStack stack = player.getItemInHand(hand);
        int fuelContained = getFuelRemaining(stack);
        int fuelToFill = getMaxFuel() - fuelContained;
        if (fuelToFill <= 0) {
            return InteractionResult.PASS;
        }

        FuelConfig cfg = FuelCanisterMod.instance().getConfig().fuels;

        var inventory = player.getInventory();
        int invSize = inventory.getContainerSize();
        for (int i = 0; i < invSize; i++) {
            fuelToFill = consumeFuel(fuelToFill, cfg, inventory, i, level);
        }

        ItemStack newStack = stack.copy();
        setFuelRemaining(newStack, getMaxFuel() - fuelToFill);

        return InteractionResult.SUCCESS.heldItemTransformedTo(newStack);
    }

    protected int consumeFuel(int fuelToFill, FuelConfig cfg, Inventory inventory, int i, Level level) {
        ItemStack possFuel = inventory.getItem(i);
        int newFuelToFill = fuelToFill;
        possFuel.getBurnTime(RecipeType.SMELTING);

        //don't want to close level here, so no try-with-resources
        //noinspection resource
        int burnTime = level.fuelValues().burnDuration(possFuel, RecipeType.SMELTING);
        if (burnTime != 0 && cfg.apply(possFuel, level.holderLookup(Registries.ITEM))) {
            int fuelEqu = burnTime / FuelEqualsBurnTime;
            if (fuelEqu == 0) {
                // Smelts less than 50 ticks, we ignore that
                return fuelToFill;
            }

            if (fuelToFill >= fuelEqu) {
                int consumedItems = fuelToFill / fuelEqu;
                consumedItems = Math.min(consumedItems, possFuel.getCount());
                if (!level.isClientSide()) {
                    inventory.removeItem(i, consumedItems);
                }
                newFuelToFill = fuelToFill - consumedItems * fuelEqu;
            }
        }
        return newFuelToFill;
    }

    /*
     * ============================================================
     *
     * Methods for Fuel/Durability handling
     *
     * ============================================================
     */

    protected int getMaxFuel() {
        return FuelCanisterMod.instance().getConfig().capacity * FuelUsedPerBurn;
    }

    protected int getFuelRemaining(ItemStack stack) {
        if (alwaysFull) {
            return getMaxFuel();
        }
        return stack.getOrDefault(CustomDataComponents.FUEL.get(), 0);
    }

    protected void setFuelRemaining(ItemStack stack, int fuel) {
        if (!alwaysFull) {
            stack.setDamageValue(stack.getMaxDamage() - (fuel / FuelUsedPerBurn));
            stack.set(CustomDataComponents.FUEL.get(), fuel);
        }
    }

    @Override
    public boolean isFoil(@NotNull ItemStack p_41453_) {
        return alwaysFull;
    }

}
