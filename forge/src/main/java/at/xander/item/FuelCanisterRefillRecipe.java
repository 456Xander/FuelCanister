package at.xander.item;

import com.mojang.serialization.MapCodec;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.codec.StreamCodec;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.*;
import net.minecraft.world.level.block.entity.FuelValues;
import org.jetbrains.annotations.NotNull;

import at.xander.FuelCanisterMod;
import at.xander.config.FuelCanisterConfiguration;
import net.minecraft.core.NonNullList;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.logging.LogManager;

public class FuelCanisterRefillRecipe implements CraftingRecipe {

	public static final Serializer SERIALIZER = new Serializer();

	private transient @Nullable FuelValues cachedFuelValues = null;

	public FuelCanisterRefillRecipe() {
		super();
	}

	@Override
	public boolean matches(CraftingInput inv, @NotNull Level worldIn) {

		// early check to abort if we don't have a fuelCanister to speed up CraftingRecipe scanning.
		var FCCount = inv.items().stream().
				filter(i -> i.getItemHolder().is(Objects.requireNonNull(FuelCanisterMod.instance().items.FuelCanister.getKey())))
				.count();
		if (FCCount != 1) {
			return false;
		}

		// cache fuel values here.
		// Note: this will only work as long as fuelValues() are the same for each dimension, otherwise we have a perfect race-condition...
		// Sadly I don't see any other way to correctly implement assemble...
		cachedFuelValues = worldIn.fuelValues();
		ItemStack fcan = null;
		int fuelFilling = 0;
		// First find the FuelCanister Item
		for (int i = 0; i < inv.size(); i++) {
			ItemStack stack = inv.getItem(i);
			if (stack.getItem() instanceof FuelCanisterItem) {
				fcan = stack;
			} else if (isValidFuel(stack, worldIn.fuelValues(), worldIn.holderLookup(Registries.ITEM))) {
				fuelFilling += getFuelValue(stack, worldIn.fuelValues());
			} else if (!stack.isEmpty()) {
				// Error
				return false;
			}
		}
		// If a FuelCanister was found => matches
		if (fcan == null) {
			return false;
		}
		FuelCanisterItem theItem = (FuelCanisterItem) fcan.getItem();
		int fuelRem = theItem.getFuelRemaining(fcan);
		int maxFuel = theItem.getMaxFuel();
		int spaceLeft = maxFuel - fuelRem;

		return spaceLeft >= fuelFilling;
	}

	private int getFuelValue(ItemStack stack, @NotNull FuelValues fv) {
		return fv.burnDuration(stack, RecipeType.SMELTING) / FuelCanisterItem.FuelEqualsBurnTime;
	}

	private boolean isValidFuel(ItemStack stack, @NotNull FuelValues fv, HolderLookup<Item> hl) {
		final FuelCanisterConfiguration config = FuelCanisterMod.instance().getConfig();
		return getFuelValue(stack, fv) > 0 && (config.craftingWithAnyFuel || config.fuels.apply(stack, hl));
	}

	@Override
	public boolean isSpecial() {
		return true;
	}

	@Override
	// Override this Method, because we don't want the FuelCanister to get left in
	// the Crafting Inventory
	public @NotNull NonNullList<ItemStack> getRemainingItems(CraftingInput inv) {
		NonNullList<ItemStack> nonnulllist = NonNullList.withSize(inv.size(), ItemStack.EMPTY);

		for (int i = 0; i < nonnulllist.size(); ++i) {
			ItemStack item = inv.getItem(i);
			ItemStack remaining = item.getCraftingRemainder();
			if (!remaining.isEmpty() && !(item.getItem() instanceof FuelCanisterItem)) {
				nonnulllist.set(i, remaining);
			}
		}

		return nonnulllist;
	}

	@Override
	public @NotNull ItemStack assemble(@NotNull CraftingInput inv, HolderLookup.@NotNull Provider reg) {
		FuelValues localFuelValues = cachedFuelValues;
		if(localFuelValues == null) {
			LogManager.getLogManager().getLogger(FuelCanisterMod.MODID).warning("Woops, no FuelValues object Found in crafting assemble.");
			return ItemStack.EMPTY;
		}
		ItemStack fcan = null;
		int fuelFilling = 0;
		// First find the FuelCanister Item
		for (int i = 0; i < inv.size(); i++) {
			ItemStack stack = inv.getItem(i);
			if (stack.getItem() instanceof FuelCanisterItem) {
				fcan = stack;
			} else if (stack.isEmpty()) {
				// Ignore empty slot
			} else if (isValidFuel(stack, localFuelValues, reg.lookupOrThrow(Registries.ITEM))) {
				fuelFilling += getFuelValue(stack, localFuelValues);
			} else {
				// Error
				// TODO log
				return ItemStack.EMPTY;
			}
		}
		// If a FuelCanister was found => matches
		if (fcan == null) {
			return ItemStack.EMPTY;
		}
		FuelCanisterItem theItem = (FuelCanisterItem) fcan.getItem();
		ItemStack resultStack = fcan.copy();
		int fuelRem = theItem.getFuelRemaining(resultStack);
		int maxFuel = theItem.getMaxFuel();
		int spaceLeft = maxFuel - fuelRem;
		if (spaceLeft < fuelFilling) {
			return ItemStack.EMPTY;
		}

		theItem.setFuelRemaining(resultStack, fuelRem + fuelFilling);

		return resultStack;
	}


	@Override
	public @NotNull RecipeSerializer<? extends CraftingRecipe> getSerializer() {
		return SERIALIZER;
	}

	@Override
	public @NotNull PlacementInfo placementInfo() {
		return PlacementInfo.NOT_PLACEABLE;
	}

	public static class Serializer implements RecipeSerializer<FuelCanisterRefillRecipe> {
		private static final FuelCanisterRefillRecipe INSTANCE = new FuelCanisterRefillRecipe();
		private static final MapCodec<FuelCanisterRefillRecipe> CODEC = MapCodec.unit(INSTANCE);
		private static final StreamCodec<RegistryFriendlyByteBuf, FuelCanisterRefillRecipe> STREAM_CODEC = StreamCodec.unit(INSTANCE);

		@Override
		public @NotNull MapCodec<FuelCanisterRefillRecipe> codec() {
			return CODEC;
		}

		@Override
		public @NotNull StreamCodec<RegistryFriendlyByteBuf, FuelCanisterRefillRecipe> streamCodec() {
			return STREAM_CODEC;
		}
	}

	@Override
	public @NotNull CraftingBookCategory category() {
		return CraftingBookCategory.MISC;
	}
}
